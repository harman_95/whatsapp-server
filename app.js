var express = require('express'),
    app = express(),
    port = process.env.PORT || 3015,
    mongoose = require('mongoose'),
    users = require('./api/models/WhatsappModel'),
    questions = require('./api/models/QuestionsModel'),
    analytics = require('./api/models/AnalyticsModel');
    bodyParser = require('body-parser'),
    https = require('https'),
    fs = require('fs'),
    multer = require('multer');

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/staging_whatsappbot');

var path = __dirname;
path = path.split('/whatsapp-chatbot');

app.use('/whatsapp-images', express.static(path[0] + '/whatsapp-images'));

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,Auth_Token,Content-Type, x-xsrf-token, x_csrftoken');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();

});

app.get('/', function (req, res) {
    res.send('Whatsapp Chatbot Running on port:' + port);
});

app.use(bodyParser.json({
    limit: '50mb',
    extended: true
}));
app.use(bodyParser.urlencoded({
    limit: '50mb',
    extended: true
}));


app.set('port', port);

var routes = require('./api/routes/Routes');
routes(app);

https.createServer({
    key: fs.readFileSync('/etc/letsencrypt/live/apps.clientify.net/privkey.pem'),
    cert: fs.readFileSync('/etc/letsencrypt/live/apps.clientify.net/cert.pem')
}, app)
    .listen(port, function () {
        console.log('Example app listening on port')
    })
    
module.exports = app;
console.log('todo list RESTful API server started on: ' + port);