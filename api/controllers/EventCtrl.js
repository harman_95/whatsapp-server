const ua = require('universal-analytics');
const bluebird = require('bluebird');


class GoogleAnalytics {
       constructor(GOOGLE_ANALYTICS_ID) {
          
           this._visitor = ua(GOOGLE_ANALYTICS_ID, 'uigyugbuygku', { strictCidFormat: false, });
   
           bluebird.promisifyAll(this._visitor);
       }
   
       sendEvent(category, action, label) {
           if (!this._visitor) return;
   
           return this._visitor.eventAsync(category, action, label);
       //     .catch(error => console.error(error));
       }
   }
   

exports.chatbotEvent = (async(req,res) =>{

       console.log(req.body.action)
       new GoogleAnalytics(req.params.ga_id).sendEvent(req.body.category, req.body.action, req.body.label)
       .then(()=>{

              res.send({
                     error: null,
                     status: 0,
                     data: "event captured successfully"
                 });
        }).catch( (error) => {
              res.send({
                     error: error,
                     status: 0,
                     data: null
                 })
        });
       
      
 })