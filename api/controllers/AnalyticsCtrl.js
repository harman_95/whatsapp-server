'use strict';
var mongoose = require('mongoose'),
    Analytic = mongoose.model('Analytic');


exports.addAnalyticData = function (req, res) {
    console.log(req.body)
    Analytic.find({
        webId: req.body.webId,
        userId: req.body.userId
    }, function (err, data) {
        if (data.length != 0) {
            Analytic.update({
                'userId': req.body.userId,
                'webId': req.body.webId,
            }, {
                $set: {
                    google_analytics_id: req.body.google_analytics_id,
                    facebook_pixel_id: req.body.facebook_pixel_id
                }
            }, {
                new: true
            }, function (err, scriptdata) {
                res.send({
                    error: null,
                    status: 0,
                    data: scriptdata
                })
            })
        } else if (data.length == 0) {
            var newAnalytic = new Analytic({
                userId: req.body.userId,
                webId: req.body.webId,
                google_analytics_id: req.body.google_analytics_id,
                facebook_pixel_id: req.body.facebook_pixel_id
            })

            console.log(newAnalytic)
            newAnalytic.save(function (err, data) {
                if (data) {
                    res.send({
                        error: null,
                        status: 0,
                        data: data
                    });
                } else {
                    res.send({
                        error: err,
                        status: 1,
                        data: null
                    });
                }
            });
        }
    })
}

exports.getAnalyticsData = function (req, res) {
    Analytic.find({
        userId: req.body.userId,
        webId: req.body.webId
    }, function (err, resdata) {

        res.send({
            error: null,
            status: 0,
            data: resdata
        })
    });
}