'use strict';
var mongoose = require('mongoose'),
    Whatsapp = mongoose.model('Whatsapp'),
    DummyWhatsappAvatarImages = mongoose.model('DummyWhatsappAvatarImages');
var multer = require('multer');
var randomstring = require("randomstring"),
    fs = require('fs'),
    Request = require('request');
    var path = __dirname;
    path = path.split('/whatsapp-chatbot');
var storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, path[0] + '/whatsapp-images');
    },
    filename: (req, file, cb) => {
        console.log(file);
        cb(null, generateName() + file.originalname);
    }
});
var upload = multer({
    storage: storage
}).single('avatar');

function generateName() {
    return randomstring.generate(7);
}
/*********GET WHATSAPP WIDGET DATA******** */
exports.getWhatsappWidget = function(req, res) {
    DummyWhatsappAvatarImages.find({}, function(err, images) {
        Whatsapp.findOne({
            user_id: req.params.userid,
            widget_id: req.params.widgetid
        }, function(err, widgets) {
            if (widgets == null) {
                var widget_whatsapp = new Whatsapp({
                    user_id: req.params.userid,
                    widget_id: req.params.widgetid,
                    widget_username: req.params.widget_username,
                    widget_name: 'Comenzar chat',
                    widget_text_style: 'inside-box',
                    widget_avatar: 'a7',
                    whatsapp_text: "Hola, necesitaría más información .....",
                    response_time_text: 'Respondemos lo antes posible',
                    welcome_message: 'Hola ✋antes de continuar déjame saber algo más  de tí ❤️',
                    phone_number: '',
                    widget_position: 'bottom-right',
                    widget_avatar_images: images,
                    hideinphone: false,
                    hideintablets: false,
                    showhidebrand: true,
                    language: "sp"
                });
                widget_whatsapp.save(function(err, whatsappdata) {
                    res.json({
                        status: 200,
                        data: whatsappdata
                    });
                })
            } else {
                res.json({
                    status: 200,
                    data: widgets
                });
            }
        });
    });
};
/*********SAVE WHATSAPP WIDGET DATA******** */
exports.saveWhatsappWidget = function(req, res) {
    Whatsapp.findOne({
        user_id: req.body.userid,
        widget_id: req.body.widgetid
    }, function(err, widgets) {
        if (widgets == null) {
            var widget_whatsapp = new Whatsapp({
                user_id: req.body.userid,
                widget_id: req.body.widgetid,
                widget_username: req.body.widget_username,
                widget_name: req.body.widget_name,
                widget_text_style: req.body.widget_text_style,
                widget_avatar: req.body.widget_avatar,
                response_time_text: req.body.response_time_text,
                welcome_message: req.body.welcome_message,
                whatsapp_text: req.body.whatsapp_text,
                phone_number: req.body.phone_number,
                widget_position: req.body.widget_position,
                widget_avatar_images: req.body.widget_avatar_images,
                hideinphone: req.body.hideinphone,
                hideintablets: req.body.hideintablets,
                language: req.body.language,
                showhidebrand: req.body.showhidebrand
            });
            widget_whatsapp.save(function(err, data) {
                res.json({
                    status: 200,
                    data: data
                });
            })
        } else {
            Whatsapp.update({
                user_id: req.body.userid,
                widget_id: req.body.widgetid
            }, {
                $set: {
                    widget_username: req.body.widget_username,
                    widget_name: req.body.widget_name,
                    widget_text_style: req.body.widget_text_style,
                    widget_avatar: req.body.widget_avatar,
                    response_time_text: req.body.response_time_text,
                    welcome_message: req.body.welcome_message,
                    whatsapp_text: req.body.whatsapp_text,
                    phone_number: req.body.phone_number,
                    widget_position: req.body.widget_position,
                    widget_avatar_images: req.body.widget_avatar_images,
                    hideinphone: req.body.hideinphone,
                    hideintablets: req.body.hideintablets,
                    language: req.body.language,
                    showhidebrand: req.body.showhidebrand
                }
            }, {
                new: true
            }, function(err, data) {
                res.json({
                    status: 201,
                    data: data
                });
            })
        }
    });
};
/****************UPLOAD PICTURE ************** */
exports.uploadWidgetAvatar = function(req, res) {
    upload(req, res, function(err) {
        if (err) {
            res.json({
                error_code: 1,
                err_desc: err
            });
            return;
        }
        res.send({
            error: null,
            status: 0,
            data: req.file.filename
        });
    });
};
/****************UPLOAD PICTURE ************** */
exports.uploadDummyAvatar = function(req, res) {
    var dummyavatars = new DummyWhatsappAvatarImages({
        avatar: req.body.image
    })
    dummyavatars.save(function(err, data) {
    })
};
exports.getbgImages = function(req, res) {
    DummyWhatsappAvatarImages.find({}, function(err, images) {
        res.json({
            images
        });
    })
}
exports.deleteAvatarImage = function(req, res) {
    fs.unlink('../whatsappimages/' + req.body.avatar, err => {
        if (err) {
            console.log(err);
        } else {
            res.json({
                status: "deleted"
            });
        }
    });
}
exports.SaveChatData = function(req, res) {
    var dat = req.body.alldata;
    Request.post({
        "headers": {
            "content-type": "application/x-www-form-urlencoded; charset=UTF-8"
        },
        "url": 'https://clientify.net/web-marketing/webforms/action/' + req.body.botid + '/',
        "body": serializeObj(JSON.parse(dat))
    }, (error, response, body) => {
        if (JSON.parse(body).success == true) {
            res.send({
                error: null,
                status: true,
                data: JSON.parse(body),
                msg: 'fetched Successfully!'
            });
        } else {
            res.send({
                error: null,
                status: false
            });
        }
    });
}

exports.validatePhone = function(req,res){
      var url = req.body.url;
      Request({url}, function (error, response, body) {
        // Do more stuff with 'body' here
        res.json({data: body});
      });
}