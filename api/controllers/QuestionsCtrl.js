'use strict';

var mongoose = require('mongoose'),
    Questions = mongoose.model('Questions');

var Questions_array = [{

        _id: 1,
        question_name: 'What is your name?',
        answer: '',
        hide: false,
        type: 'Name',
        set_as: 'name',
        required: true,
        icon: 'fa fa-user',
        skip: false,
        userrp: {
            msg: '',
            id: 1
        }

    }, {

        _id: 2,
        question_name: 'What is your phone number?',
        answer: '',
        hide: false,
        type: 'Phone',
        set_as: 'phone',
        required: false,
        show_error_message: false,
        error_message: 'Phone Number you have entered is not valid',
        icon: 'fa fa-phone',
        skip: false,
        userrp: {
            msg: '',
            id: 2
        }

    }, {

        _id: 3,
        question_name: 'What is your email address?',
        answer: '',
        hide: false,
        show_error_message: false,
        type: 'Email',
        set_as: 'email',
        required: false,
        error_message: 'Email Address is not valid',
        corporate_email: false,
        corporate_error_message: 'DNS MX not found',
        corporate_error: false,
        icon: 'fa fa-envelope',
        skip: false,
        userrp: {
            msg: '',
            id: 3
        }

    }

]

/*********GET WHATSAPP WIDGET DATA******** */
exports.getQuestionsList = function(req, res) {
    Questions.findOne({
        'user_id': req.params.userid,
        'widget_id': req.params.widgetid
    }, function(err, questiondata) {
        if (questiondata == null) {
            res.json({
                status: 201,
                data: questiondata
            });
        } else if (questiondata != null) {
            res.json({
                status: 200,
                data: questiondata
            });
        }
    })

};



/*********SAVE WHATSAPP WIDGET DATA by userid and questionid********* */
exports.editQuestions = function(req, res) {
    Questions.findOne({
        user_id: req.body.userid,
        widget_id: req.body.widgetid
    }, function(err, questions) {
        if (questions == null) {
            var new_Questions = Questions({
                user_id: req.body.userid,
                widget_id: req.body.widgetid,
                Questions: req.body.questions
            })
            new_Questions.save(function(err, questions) {
                if (err) {
                    res.json({
                        status: 500,
                        data: null,
                        error: "Internal Server Error"
                    });
                } else {
                    res.json({
                        status: 200,
                        data: questions
                    });
                }
            })
        } else {
            Questions.update({
                user_id: req.body.userid,
                widget_id: req.body.widgetid
            }, {
                $set: {
                    Questions: req.body.questions
                }
            }, {
                new: true
            }, function(err, data) {
                res.json({
                    status: 201,
                    data: data
                });
            })
        }
    });

};