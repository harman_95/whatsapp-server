'use strict';
module.exports = function(app) {

var widgets = require('../controllers/WhatsappCtrl');

    app.route('/api/v1/getWhatsappWidget/:userid/:widgetid/:widget_username')
    .get(widgets.getWhatsappWidget)

    app.route('/api/v1/getWhatsappWidgetbotdata/:userid/:widgetid')
    .get(widgets.getWhatsappWidget)

    app.route('/api/v1/saveWhatsappWidget')
    .post(widgets.saveWhatsappWidget)

    app.route('/api/v1/uploadWidgetAvatar')
    .post(widgets.uploadWidgetAvatar)

    app.route('/api/v1/uploadDummyAvatar')
    .post(widgets.uploadDummyAvatar)

    app.route('/api/v1/getbgImages')
    .get(widgets.getbgImages)

    app.route('/api/v1/deleteAvatarImage')
    .post(widgets.deleteAvatarImage)

    app.route('/api/v1/SaveChatData')
    .post(widgets.SaveChatData)

    app.route('/api/v1/validatePhone')
    .post(widgets.validatePhone)


var questions = require('../controllers/QuestionsCtrl');
    
    app.route('/api/v1/getQuestionsList/:userid/:widgetid')
    .get(questions.getQuestionsList)

    app.route('/api/v1/editQuestions')
    .post(questions.editQuestions)

var events = require('../controllers/EventCtrl')
    app.route('/chatbot_events/:ga_id')
    .post(events.chatbotEvent)

var analytic = require('../controllers/AnalyticsCtrl')
    app.route('/api/v1/analytics')
        .post(analytic.addAnalyticData)
    app.route('/api/v1/getanalytics')
        .post(analytic.getAnalyticsData)  
     
};


