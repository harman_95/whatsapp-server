'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var AnalyticSchema = new Schema({

    userId : {
        type:String
      },
    webId : {
        type:String
      },
      
    google_analytics_id:{
         type:String
      },
    facebook_pixel_id:{
         type:String
      },
    created_at: { type: Date, default: Date.now },
    updated_at: { type: Date, default: Date.now }

});

module.exports = mongoose.model('Analytic', AnalyticSchema);