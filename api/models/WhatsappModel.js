'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var WhatsappSchema = new Schema({


    user_id : {
        type: String
    },
    widget_id : {
      type: String
    },
    widget_username : {
      type: String , default : 'Mercedes'
    },
    widget_name : {
      type: String , default : 'Comenzar chat'
    },
    widget_text_style : {
      type: String, default : 'inside-box'
    },
    widget_avatar: {
      type: String, default : 'https://apps.clientify.net:3005/whatsappimages/a7'
    },
    widget_avatar_images : {
      type: Array
    },
    whatsapp_text : {
      type : String , default : 'Hola, necesitaría más información .....'
    },
    response_time_text : {
      type : String , default : 'Respondemos lo antes posible'
    },
    welcome_message : {
      type : String , default : 'Hola ✋antes de continuar déjame saber algo más  de tí ❤️'
    },
    phone_number : {
      type : Object , default : ''
    },
    widget_position : {
      type : String , default :  'bottom-right'
    },
    language : {
      type : String , default :  'sp'
    },
    showhidebrand : {
      type : Boolean , default : true
    },
    hideinphone : {
      type : Boolean , default : false
    },
    hideintablets : {
      type : Boolean , default : false
    },
    created_at: { type: Date, default: Date.now },
    updated_at: { type: Date, default: Date.now }

 
});


var dummyWhatsappAvatarSchema = new Schema({

  avatar: {
    type: String
  },

  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }

});

module.exports = mongoose.model('DummyWhatsappAvatarImages', dummyWhatsappAvatarSchema);
module.exports = mongoose.model('Whatsapp', WhatsappSchema);