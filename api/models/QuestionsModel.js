'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var QuestionsSchema = new Schema({


    user_id : {
        type: String
    },
    widget_id : {
      type: String
    },
    Questions : {
        type : Array
    },
    created_at: { type: Date, default: Date.now },
    updated_at: { type: Date, default: Date.now }

 
});




module.exports = mongoose.model('Questions', QuestionsSchema);